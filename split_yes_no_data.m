function [ yes_cell, no_cell ] = get_yes_no_data( filename )

    data_struct = load(filename);
    data_fields = fieldnames(data);
    marker_struct = data_struct.evt_User_Markup;
    
    % following two lines are required to correct markings in 'Ravi_1_20170629_044910_fil.mat'
    %new_struct = marker_struct(:, 1:180);
    %new_struct(:, 181:240) = marker_struct(:, 183:242);
    %marker_struct = new_struct;
    
    samples = 40;
    yes_cell = cell(samples, 1);
    no_cell = cell(samples, 1);
    no_index = 1;
    yes_index = 1;
    for i =  3:3:240
        str = marker_struct{1, i};
        start_index = marker_struct{4, i-1};
        end_index = marker_struct{4, i};
        trial_no = marker_struct{3, i};
        current_data = data_struct.(data_fields{trial_no});
        if(strcmp(str, 'NoE_') && no_index <= samples)
            no_cell{no_index, 1} = current_data(:, start_index:end_index);
            no_index = no_index + 1;
        elseif(yes_index <= samples)
            yes_cell{yes_index, 1} = current_data(:, start_index:end_index);
            yes_index = yes_index + 1;
        end
    end


end

